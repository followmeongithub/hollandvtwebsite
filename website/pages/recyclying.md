---
layout              : page
title               : "Recycling"
meta_title          : "Recycling Information"
subheadline         : "Recycle"
teaser              : "Information and schedule for Town Recycling"
header              :
  image_fullwidth   : header_school_field.png
permalink           : "/recycling/"
---
- [Recycling Info](https://www.nekwmd.org/_files/ugd/30ad8a_0cc4335c0a7b4477abfbf0dcbd169297.pdf)
- [Northeast Kingdom Waste Management Dist. (Recycle, Compost, Waste)](https://www.nekwmd.org/)

