---
layout              : page
show_meta           : false
title               : "Dog License"
subheadline         : ""
teaser              : "Information on dog licenses."
header:
   image_fullwidth  : "header_holland_pond_rainbow.png"
permalink           : "/documents-and-resources/dog-license/"
---
<a href="{{ site.url }}{{ site.baseurl }}/assets/documents/ordinances/Dog Ordinance.pdf">Dog Ordinance</a>


