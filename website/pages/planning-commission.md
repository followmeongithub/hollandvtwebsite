---
layout              : page
show_meta           : false
title               : "Planning Commission"
subheadline         : ""
teaser              : "Minutes for the Planning Commission."
header:
   image_fullwidth  : "header_holland_pond_rainbow.png"
permalink           : "/minutes-and-agendas/planning-commission/"
---
<ul>
    {% assign static_files = site.static_files %}
    {% for file in static_files reversed %}
      {% if file.planning_commission %}
        <li><a href="{{ site.url }}{{ site.baseurl }}{{ file.path }}">{{ file.name }}</a></li>
      {% endif %}
    {% endfor %}
</ul>

