---
layout              : page
show_meta           : false
title               : "Holland Community Center"
subheadline         : ""
teaser              : ""
header:
   image_fullwidth  : "header_community_parade.png"
permalink           : "/documents-and-resources/holland-community-center/"
---


![Community Music Event](/images/community_music.png)
![Garden H](/images/garden.png)
