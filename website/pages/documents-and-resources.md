---
layout              : page
show_meta           : false
title               : "Documents and Resources"
subheadline         : ""
teaser              : "A collection of town related documents."
header:
   image_fullwidth  : "header_holland_pond_rainbow.png"
permalink           : "/documents-and-resources/"
---
<ul>
    {% assign static_files = site.static_files %}
    {% for file in static_files reversed %}
      {% if file.document %}
        <li><a href="{{ site.url }}{{ site.baseurl }}{{ file.path }}">{{ file.name }}</a></li>
      {% endif %}
    {% endfor %}
</ul>

