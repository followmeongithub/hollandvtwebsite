---
layout              : page
show_meta           : false
title               : "Cemetery Commission"
subheadline         : ""
teaser              : "Minutes for the Cemetery Commission."
header:
   image_fullwidth  : "header_holland_pond_rainbow.png"
permalink           : "/minutes-and-agendas/cemetery-commission/"
---
<ul>
    {% assign static_files = site.static_files %}
    {% for file in static_files reversed %}
      {% if file.cemetery_commission %}
        <li><a href="{{ site.url }}{{ site.baseurl }}{{ file.path }}">{{ file.name }}</a></li>
      {% endif %}
    {% endfor %}
</ul>

