---
layout              : page
show_meta           : false
title               : "Select Board"
subheadline         : ""
teaser              : "Minutes for the Town Select Board."
header:
   image_fullwidth  : "header_holland_pond_rainbow.png"
permalink           : "/minutes-and-agendas/select-board/"
---
<ul>
    {% assign static_files = site.static_files %}
    {% for file in static_files reversed %}
      {% if file.select_board %}
        <li><a href="{{ site.url }}{{ site.baseurl }}{{ file.path }}">{{ file.name }}</a></li>
      {% endif %}
    {% endfor %}
</ul>

