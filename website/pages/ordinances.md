---
layout              : page
show_meta           : false
title               : "Ordinances"
subheadline         : ""
teaser              : "A collection of town ordinances."
header:
   image_fullwidth  : "header_holland_pond_rainbow.png"
permalink           : "/documents-and-resources/ordinances/"
---
<ul>
    {% assign static_files = site.static_files %}
    {% for file in static_files reversed %}
      {% if file.ordinance %}
        <li><a href="{{ site.url }}{{ site.baseurl }}{{ file.path }}">{{ file.name }}</a></li>
      {% endif %}
    {% endfor %}
</ul>

