---
layout              : page
show_meta           : false
title               : "School Board"
subheadline         : ""
teaser              : "Minutes for the School Board."
header:
   image_fullwidth  : "header_school_gym.png"
permalink           : "/minutes-and-agendas/school-board/"
---
<ul>
    {% assign static_files = site.static_files %}
    {% for file in static_files reversed %}
      {% if file.school_board %}
        <li><a href="{{ site.url }}{{ site.baseurl }}{{ file.path }}">{{ file.name }}</a></li>
      {% endif %}
    {% endfor %}
</ul>

