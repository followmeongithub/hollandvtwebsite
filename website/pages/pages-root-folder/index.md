---
#
# Use the widgets beneath and the content will be
# inserted automagically in the webpage. To make
# this work, you have to use › layout: frontpage
#
layout: frontpage
header:
  image_fullwidth: header_holland_pond_rainbow.png
widget1:
  title: "Minutes & Agendas"
  url: '/minutes-and-agendas/'
  image: widget-agenda_303x182_glenn-carstens-peters-RLw-UC03Gwc-unsplash.png
  text: '<em>Minutes and Agendas</em> for town boards and commissions'
widget2:
  title: "Documents & Resources"
  url: 'https://hollandvt.bbbiz.us/documents-and-resources/'
  image: widget-documents_303-182_wesley-tingey-snNHKZ-mGfE-unsplash.png
  text: 'A collections of <em>Documents and Resources</em> related to the Town of Holland.'
widget3:
  title: "Recycling"
  url: 'https://hollandvt.bbbiz.us/recycling/'
  image: widget-recycling_303x182_pawel-czerwinski-RkIsyD_AVvc-unsplash.png
  text: '<em>Recycling</em> schedule and information.'
#
# Use the call for action to show a button on the frontpage
#
# To make internal links, just use a permalink like this
# url: /getting-started/
#
# To style the button in different colors, use no value
# to use the main color or success, alert or secondary.
# To change colors see sass/_01_settings_colors.scss
#
callforaction:
  url: https://hollandvt.bbbiz.us/forms/call-outs/
  text: Add me to future Email call-outs ›
  style: alert
permalink: /index.html
#
# This is a nasty hack to make the navigation highlight
# this page as active in the topbar navigation
#
homepage: true
---


